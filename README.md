# lucerne

gRPC service for scylla integration (insert/select locations)

## Usage/Examples

In client/main.go

## Deployment

To deploy this project run

```bash
helm uninstall $(basename "$PWD") -n $(basename "$PWD"); docker build --ssh default=~/.ssh/id_rsa -t registry.gitlab.com/assembly4/$(basename "$PWD"):latest .;helm upgrade $(basename "$PWD") ./chart/ -i --history-max 0 -n $(basename "$PWD");
```

## Environment Variables

To run this project, you will need to deploy terraform which provisions needed secrets and configmaps - [link to repo](https://gitlab.com/assembly4/terraform)