package main

import (
	"context"
	"crypto/tls"
	"io"
	"log"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"gitlab.com/assembly4/bern"
	pb "gitlab.com/assembly4/proto/lucerne"
)

const (
	address  = "lucerne.assembly4.tk:443"
	loginUrl = "http://geneva.assembly4.tk/api/v1/login"
)

var (
	opts []grpc.DialOption
	c    pb.LocationClient
)

func ins(token string, deviceUuid string, deviceLocs *[]*pb.Loc) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(1*time.Minute))
	defer cancel()

	newDeviceLoc := &pb.DeviceLocs{
		DeviceId: deviceUuid,
		Token:    token,
		Loc:      *deviceLocs,
	}

	start := time.Now()
	r, err := c.InsertLoc(ctx, newDeviceLoc)
	if err != nil {
		log.Fatalf("could not insert loc: %v", err)
	}
	elapsed := time.Since(start)
	log.Printf("| %s | %s ", elapsed, r.GetStatus())
}

func sel(token string, deviceUuid string, start string, end string) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(1*time.Minute))
	defer cancel()

	timeRange := pb.DeviceTimeRange{
		DeviceId: deviceUuid,
		Token:    token,
		Start:    start,
		End:      end,
	}

	stream, err := c.SelectLoc(ctx, &timeRange)
	if err != nil {
		log.Fatalf("%v.SelectLocs(_) = _, %v", c, err)
	}
	for {
		locs, err := stream.Recv()
		if err == io.EOF {
			log.Println("end of stream")
			break
		}
		if err != nil {
			log.Fatalf("%v.SelectLocs(_) = _, %v", c, err)
		}
		log.Println(locs)
	}
}

func main() {
	tc := credentials.NewTLS(&tls.Config{
		InsecureSkipVerify: true,
	})
	opts = append(opts, grpc.WithTransportCredentials(tc))

	conn, err := grpc.Dial(address, opts...)
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c = pb.NewLocationClient(conn)

	//locs := []*pb.Loc{
	//	{Lat: 5, Long: -1, Time: "2021-08-31T12:10:50Z"},
	//	{Lat: 0, Long: 0, Time: "2021-08-30T12:10:51Z"},
	//	{Lat: 2, Long: 4, Time: "2021-08-31T12:10:52Z"},
	//	{Lat: -0, Long: 0, Time: "2021-08-31T12:10:53Z"},
	//	{Lat: 0, Long: -0, Time: "2021-08-31T12:10:54Z"},
	//}

	token, err := bern.Login(loginUrl, "user1@domain.com", "superpass1")
	if err != nil {
		log.Println(err)
	}

	//go ins(token, "4e34d998-0f39-41e9-875a-1208811f5afa", &locs)
	//go ins(token, "4e34d998-0f39-41e9-875a-1208811f5afa", &locs)
	//go ins(token, "591a45bf-8d71-475c-b9dd-9750aeba8e21", &locs)
	//go ins(token, "591a45bf-8d71-475c-b9dd-9750aeba8e21", &locs)
	sel(token, "4e34d998-0f39-41e9-875a-1208811f5afa", "2021-08-27T12:10:50Z", time.Now().UTC().Format(time.RFC3339))

	time.Sleep(1 * time.Minute)
}
