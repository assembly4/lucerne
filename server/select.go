package main

import (
	"fmt"
	"time"

	"github.com/rs/zerolog/log"

	"gitlab.com/assembly4/bern"
	pb "gitlab.com/assembly4/proto/lucerne"
	"gitlab.com/assembly4/scylla"
)

func (s *LocationServer) SelectLoc(trd *pb.DeviceTimeRange, stream pb.Location_SelectLocServer) error {
	defer bern.TimeTrack(time.Now(), "select locs for "+trd.GetDeviceId())

	var ch = make(chan []scylla.LocationModel)

	token := trd.GetToken()
	id := trd.GetDeviceId()

	// check if user is authorized to read device loc
	if _, err := bern.Authorized(token, map[string]bool{id: true}, false, publicKey); err != nil {
		log.Error().Err(err).Send()
		return err
	}
	allowedDevices, err := bern.Authorized(token, map[string]bool{id: true}, false, publicKey)
	if err != nil {
		log.Error().Err(err).Send()
		return err
	}
	// check if requested device is in allowedDevices from database
	if val, ok := allowedDevices[id]; !val || !ok {
		return fmt.Errorf("unauthorized")
	}

	// cast input string to time.Time format
	start, err := time.Parse(time.RFC3339, trd.GetStart())
	if err != nil {
		log.Error().Err(err).Send()
		return err
	}

	// set end in time range to time.Now()
	if len(trd.End) == 0 {
		trd.End = time.Now().Format(time.RFC3339)
	}
	end, err := time.Parse(time.RFC3339, trd.End)
	if err != nil {
		log.Error().Err(err).Send()
		return err
	}

	trdModel := scylla.DeviceTimeRange{UUID: trd.DeviceId, Start: start.UTC(), End: end.UTC()}

	// validate data
	if err := validate.Struct(trdModel); err != nil {
		log.Error().Err(err).Send()
		return err
	}

	// select query that pushes each page into ch channel
	go scylla.SelectQuery(session, trdModel, ch)

	// listen on channel, tranform data and send to stream
	for {
		locs, more := <-ch
		locBatch := []*pb.Loc{}
		for _, l := range locs {
			locBatch = append(locBatch, &pb.Loc{Time: l.Time.Format(time.RFC3339), Lat: l.Lat, Long: l.Long})
		}

		if !more {
			break
		}
		if err := stream.Send(&pb.DeviceLocs{Loc: locBatch}); err != nil {
			log.Error().Err(err).Send()
			return err
		}
	}

	return nil
}
