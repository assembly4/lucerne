package main

import (
	"crypto/rsa"
	"net"
	"os"

	"github.com/go-playground/validator/v10"
	"github.com/gocql/gocql"
	"google.golang.org/grpc"
	"github.com/rs/zerolog/log"

	"gitlab.com/assembly4/bern"
	pb "gitlab.com/assembly4/proto/lucerne"
	"gitlab.com/assembly4/proto/zug"
	"gitlab.com/assembly4/scylla"
)

var (
	publicKey *rsa.PublicKey
	session   *gocql.Session
	zugConn   zug.RedisClient
	validate  = validator.New()
)

const port = ":50123"

type LocationServer struct {
	pb.UnimplementedLocationServer
}

func main() {
	requiredEnv := []string{"PUBLIC_KEY", "LOG_LEVEL", "CQLSH_HOST", "SCYLLA_KEYSPACE", "ZUG_FQDN", "GENEVA_FQDN", "DEVICES_ENDPOINT"}
	bern.CheckEnvVars(requiredEnv)
	bern.SetupLogger()

	session = scylla.CreateSession()
	defer session.Close()

	var (
		opts []grpc.DialOption
		err  error
	)

	publicKey, err = bern.LoadPublicKey(os.Getenv("PUBLIC_KEY"))
	if err != nil {
		log.Fatal().Msgf("unable to load public key: %v", err)
	}

	opts = append(opts, grpc.WithInsecure())
	conn, err := grpc.Dial(os.Getenv("ZUG_FQDN"), opts...)
	if err != nil {
		log.Fatal().Msgf("unable to connect to zug service: %v", err)
	}
	defer conn.Close()
	zugConn = zug.NewRedisClient(conn)

	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Panic().Msgf("failed to listen: %v", err)
	}

	s := grpc.NewServer()
	pb.RegisterLocationServer(s, &LocationServer{})
	log.Printf("server listening at %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		log.Fatal().Msgf("failed to serve: %v", err)
	}
}
