package main

import (
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/rs/zerolog/log"

	"gitlab.com/assembly4/bern"
	pb "gitlab.com/assembly4/proto/lucerne"
	"gitlab.com/assembly4/proto/zug"
	"gitlab.com/assembly4/scylla"
)

type marshalledData struct {
	Token string                 `validate:"required,jwt"`
	Data  []scylla.LocationModel `validate:"required,max=500,dive"`
}

type result struct {
	Service string
	Result  string
}

func (s *LocationServer) InsertLoc(ctx context.Context, in *pb.DeviceLocs) (*pb.Status, error) {
	defer bern.TimeTrack(time.Now(), "inserting locs for "+in.GetDeviceId())

	var (
		device         scylla.Device
		loc            scylla.LocationModel
		marshalledData marshalledData
		wg             sync.WaitGroup
		res            result
		results        []result
		resultsCh      = make(chan result, 2)
	)

	//marshal data
	token := in.GetToken()
	locs := in.GetLoc()
	device.Id = in.GetDeviceId()

	// check if user is authorized to write device loc
	allowedDevices, err := bern.Authorized(token, map[string]bool{device.Id: true}, true, publicKey)
	if err != nil {
		return &pb.Status{Status: "error"}, err
	}
	// check if requested device is in allowedDevices from database
	if val, ok := allowedDevices[device.Id]; !val || !ok {
		return &pb.Status{Status: "unauthorized"}, fmt.Errorf("unauthorized")
	}

	marshalledData.Token = token

	latest := scylla.LocationModel{
		Time: time.Date(0, 0, 0, 0, 0, 0, 0, time.UTC),
		Lat:  0,
		Long: 0,
	}
	for _, l := range locs {
		t, _ := time.Parse(time.RFC3339, l.Time)
		loc.Time = t.UTC()
		loc.Lat = l.Lat
		loc.Long = l.Long
		marshalledData.Data = append(marshalledData.Data, loc)

		if t.After(latest.Time) {
			latest = loc
		}
	}

	// validate data
	if err := validate.Struct(device); err != nil {
		log.Error().Err(err)
		return &pb.Status{Status: "error"}, err
	}
	if err := validate.Struct(marshalledData); err != nil {
		log.Error().Err(err)
		return &pb.Status{Status: "error"}, err
	}

	// only one task is mandatory - scyllaTask
	wg.Add(1)
	go zugTask(&wg, resultsCh, device, latest)
	go scyllaTask(&wg, resultsCh, device, marshalledData.Data)
	wg.Wait()

	for {
		if len(resultsCh) == 0 {
			break
		}
		res = <-resultsCh
		results = append(results, res)
	}

	return &pb.Status{Status: bern.Prettify(results)}, nil
}

func pub(uuid string, loc scylla.LocationModel) (string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	r, err := zugConn.PublishDeviceLoc(ctx, &zug.Location{
		DeviceId: uuid,
		Time:     loc.Time.Format(time.RFC3339),
		Lat:      loc.Lat,
		Long:     loc.Long,
	})
	if err != nil {
		log.Warn().Msgf("could not publish loc: %v", err)
		return "", err
	}

	return r.Status, nil
}

func zugTask(wg *sync.WaitGroup, ch chan result, device scylla.Device, loc scylla.LocationModel) {
	defer bern.TimeTrack(time.Now(), "zug publish")
	//defer wg.Done() // if mandatory

	r, err := pub(device.Id, loc)
	if err != nil {
		log.Error().Err(err)
		r = err.Error()
	}
	ch <- result{
		Service: "redis",
		Result:  r,
	}
}

func scyllaTask(wg *sync.WaitGroup, ch chan result, device scylla.Device, data []scylla.LocationModel) {
	defer bern.TimeTrack(time.Now(), "scylla insert")
	defer wg.Done()

	r := "ok"
	if err := scylla.InsertBatchQuery(session, device, data); err != nil {
		log.Error().Err(err).Send()
		r = err.Error()
	}
	ch <- result{
		Service: "scylla",
		Result:  r,
	}
}
